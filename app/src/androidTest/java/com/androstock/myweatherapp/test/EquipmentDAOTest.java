package com.androstock.myweatherapp.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.androstock.myweatherapp.controller.database.EquipmentDAO;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Home;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EquipmentDAOTest {

    // Le nom du fichier qui représente ma base
    protected final static String DBNAME = "dbtest.db";

    EquipmentDAO db;
    Context context;
    Home homeTest;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
        db = new EquipmentDAO(context,DBNAME);
        db.open();

    }

    @After
    public void tearDown() {
        db.close();
        context.deleteDatabase(DBNAME);
    }

    @Test
    public void addTest(){
        db.add("eq1","light",false,false,0);
        List<Equipment> list = db.getAllEquipments();
        assertEquals(1, list.size());

        db.add("eq2","light",false,false,0);
        list = db.getAllEquipments();

        assertEquals(2, list.size());
        assertTrue(list.get(0).getId()!=list.get(1).getId());
    }

    @Test
    public void deleteTest(){
        db.add("eq1","light",false,false,0);
        db.delete(db.getLastEquipment());
        List<Equipment> list = db.getAllEquipments();
        assertEquals(0, list.size());
    }

    @Test
    public void FunctionalTest(){

        db.add("eq1","light",false,false,0);
        db.delete(db.getLastEquipment());
        db.add("eq2","light",false,false,0);
        db.add("eq1","light",false,false,0);
        db.delete(db.getLastEquipment());
        db.delete(db.getLastEquipment());

        List<Equipment> list = db.getAllEquipments();
        assertEquals(0, list.size());
    }
}