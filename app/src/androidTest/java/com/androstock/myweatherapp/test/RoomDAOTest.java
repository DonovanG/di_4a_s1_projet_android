package com.androstock.myweatherapp.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.androstock.myweatherapp.controller.database.RoomDAO;
import com.androstock.myweatherapp.model.Room;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RoomDAOTest {

    // Le nom du fichier qui représente ma base
    protected final static String DBNAME = "dbtest.db";

    RoomDAO db;
    Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
        db = new RoomDAO(context,DBNAME);
        db.open();
    }

    @After
    public void tearDown() {
        db.close();
        context.deleteDatabase(DBNAME);
    }

    @Test
    public void initTest(){
        List<Room> list = db.getRooms();
        assertEquals(0, list.size());
    }

    @Test
    public void addTest(){
        db.add("saloon");
        List<Room> list = db.getRooms();
        assertEquals(1, list.size());

        db.add("room1");

        list = db.getRooms();

        assertEquals(2, list.size());
        assertTrue(list.get(0).getId()!=list.get(1).getId());
    }

    @Test
    public void deleteTest(){
        db.add("saloon");
        db.delete(db.getLastRoomId());
        List<Room> list = db.getRooms();
        assertEquals(0, list.size());
    }

    @Test
    public void FunctionalTest(){

        db.add("saloon");
        db.delete(db.getLastRoomId());
        db.add("room1");
        db.add("saloon");
        db.delete(db.getLastRoomId());
        db.delete(db.getLastRoomId());

        List<Room> list = db.getRooms();
        assertEquals(0, list.size());
    }
}