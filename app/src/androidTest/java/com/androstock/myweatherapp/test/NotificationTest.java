package com.androstock.myweatherapp.test;

import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Notification;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NotificationTest {

    Notification n;

    @Before
    public void setUp() {
        n = new Notification(0, "close the blinder",
                new Equipment(0, "eq0", "blinder", true, true));
    }

    @After
    public void tearDown() {
        n = null;
    }

    @Test
    public void getId() {
        assertEquals(n.getId(), 0);
    }

    @Test
    public void getMsg() {
        assertEquals(n.getMsg(), "close the blinder");
    }


    @Test
    public void getEquipment() {
        assertEquals(n.getEquipment().getId(), 0);
    }
}