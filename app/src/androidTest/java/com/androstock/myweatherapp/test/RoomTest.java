package com.androstock.myweatherapp.test;

import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RoomTest {

    Room r;

    @Before
    public void setUp() {
        r = new Room(0,"r0");
    }

    @After
    public void tearDown() {
        r = null;
    }

    @Test
    public void getId() {
        assertEquals(r.getId(),0);
    }

    @Test
    public void getName() {
        assertEquals(r.getName(), "r0");
    }

    @Test
    public void addEquipment() {
        assertEquals(r.getEquipments().size(), 0);
        r.addEquipment(new Equipment(0,"eq0","light", false, false));
        assertEquals(r.getEquipments().size(), 1);
        assertEquals(r.getEquipments().get(0).getId(), 0);

    }

    @Test
    public void deleteEquipment() {
        assertEquals(r.getEquipments().size(), 0);
        r.addEquipment(new Equipment(0,"eq0","light", false, false));
        assertEquals(r.getEquipments().size(), 1);
        r.deleteEquipment(r.getEquipments().get(0));
        assertEquals(r.getEquipments().size(), 0);

    }
}