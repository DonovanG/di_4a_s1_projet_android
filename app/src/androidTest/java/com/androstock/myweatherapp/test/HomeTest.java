package com.androstock.myweatherapp.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.androstock.myweatherapp.controller.database.EquipmentDAO;
import com.androstock.myweatherapp.controller.database.NotificationDAO;
import com.androstock.myweatherapp.controller.database.RoomDAO;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Home;
import com.androstock.myweatherapp.model.Room;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HomeTest {

    // Le nom du fichier qui représente ma base
    protected final static String DBNAME = "dbtest.db";
    Context context;

    RoomDAO rdb;
    EquipmentDAO edb;
    NotificationDAO ndb;


    Home h;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
        h = new Home(context, DBNAME);

        rdb = new RoomDAO(context, DBNAME);
        edb = new EquipmentDAO(context, DBNAME);
        ndb = new NotificationDAO(context, DBNAME);

    }

    @After
    public void tearDown() {
        context.deleteDatabase(DBNAME);
        h = null;
        rdb = null;
        edb = null;
        ndb = null;
    }

    @Test
    public void initTest(){
        assertEquals(h.getRoom().size(), 0);
        assertEquals(h.getNotifs().size(), 0);
        assertEquals(h.getEquipments().size(), 0);

    }


    @Test
    public void addRoom() {
        h.addRoom("r0");

        assertEquals(rdb.getRooms().size(), 1);
        assertEquals(h.getRoom().size(), 1);
        assertEquals(rdb.getLastRoomId(), h.getRoom().get(0).getId());

        h.addRoom("r1");

        assertEquals(rdb.getRooms().size(), 2);
        assertEquals(h.getRoom().size(), 2);
        assertEquals(rdb.getLastRoomId(), h.getRoom().get(1).getId());

    }

    @Test
    public void deleteRoom() {
        h.addRoom("r0");

        assertEquals(rdb.getRooms().size(), 1);
        assertEquals(h.getRoom().size(), 1);
        assertEquals(rdb.getLastRoomId(), h.getRoom().get(0).getId());

        h.addRoom("r1");

        assertEquals(rdb.getRooms().size(), 2);
        assertEquals(h.getRoom().size(), 2);
        assertEquals(rdb.getLastRoomId(), h.getRoom().get(1).getId());

        Room r = h.getRoomById(rdb.getLastRoomId());

        h.deleteRoom(r);

        assertEquals(rdb.getRooms().size(), 1);
        assertEquals(h.getRoom().size(), 1);
        assertEquals(rdb.getLastRoomId(),h.getRoom().get(0).getId());

        r = h.getRoomById(rdb.getLastRoomId());

        h.deleteRoom(r);

        assertEquals(rdb.getRooms().size(), 0);
        assertEquals(h.getRoom().size(), 0);
    }

    @Test
    public void getRoomById() {

        h.addRoom("r1");
        int id1 = h.getRoom().get(0).getId();
        h.addRoom("r2");
        int id2 = h.getRoom().get(1).getId();

        Room r1 = h.getRoomById(id1);
        assertEquals(r1.getId(),id1);
        assertEquals(r1.getName(), "r1");

        Room r2 = h.getRoomById(id2);
        assertEquals(r2.getId(),id2);
        assertEquals(r2.getName(), "r2");

    }

    @Test
    public void getRoomByEquimentId() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();
        h.addRoom("r1");
        int idr1 = h.getRoom().get(1).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        int ide0 = h.getRoom().get(0).getEquipments().get(0).getId();
        h.addEquipment("eq1","light",false,false,idr0);
        int ide1 = h.getRoom().get(0).getEquipments().get(1).getId();
        h.addEquipment("eq2","light",false,false,idr1);
        int ide2 = h.getRoom().get(1).getEquipments().get(0).getId();

        Room r0 = h.getRoomByEquimentId(ide1);
        Room r1 = h.getRoomByEquimentId(ide2);

        assertNotEquals(r0,r1);
        assertEquals(r0.getName(),"r0");
        assertEquals(r1.getName(),"r1");



    }

    @Test
    public void getEquipmentById() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        h.addEquipment("eq1","light",false,false,idr0);
        int id = edb.getLastEquipment();
        h.addEquipment("eq2","light",false,false,idr0);

        Equipment e = h.getEquipmentById(id);

        assertEquals(e, h.getRoom().get(0).getEquipments().get(1));
        assertEquals(e.getId(), id);

    }

    @Test
    public void addEquipment() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        h.addEquipment("eq1","light",false,false,idr0);
        int ide1 = edb.getLastEquipment();
        h.addEquipment("eq2","light",false,false,idr0);

        assertEquals(h.getEquipments().size(), 3);
        assertEquals(edb.getAllEquipments().size(), 3);

        assertEquals(edb.getEquipment(ide1).getId(), h.getRoom().get(0).getEquipments().get(1).getId());
        assertEquals(edb.getEquipmentI(3).getId(), h.getRoom().get(0).getEquipments().get(2).getId());
    }

    @Test
    public void deleteEquipment() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        h.addEquipment("eq1","light",false,false,idr0);
        int ide1 = edb.getLastEquipment();
        h.addEquipment("eq2","light",false,false,idr0);
        h.addEquipment("eq3","light",false,false,idr0);


        h.deleteEquipment(h.getRoom().get(0).getEquipments().get(1));

        assertEquals(h.getEquipments().size(), 3);
        assertEquals(edb.getAllEquipments().size(), 3);

        h.deleteEquipment(h.getRoom().get(0).getEquipments().get(2));
        assertEquals(h.getEquipments().size(), 2);
        assertEquals(edb.getAllEquipments().size(), 2);

        h.addEquipment("eq1","light",false,false,idr0);
        int ide1Bis = edb.getLastEquipment();
        assertEquals(h.getEquipments().size(), 3);
        assertEquals(edb.getAllEquipments().size(), 3);
        assertEquals(h.getEquipmentById(ide1), null);
        assertEquals(h.getEquipmentById(ide1Bis),h.getRoom().get(0).getEquipments().get(2));

    }

    @Test
    public void addNotification() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        int ideq0 = edb.getLastEquipment();
        h.addEquipment("eq1","light",false,false,idr0);
        int ideq1 = edb.getLastEquipment();

        h.addNotification("turn off the light", ideq0);
        h.addNotification("turn on the light", ideq0);
        h.addNotification("turn on the light", ideq1);

        assertEquals(h.getNotifs().size(), 3);
        assertEquals(ndb.getAllNotifs(h).size(),3);

    }

    @Test
    public void deleteNotification() {
        h.addRoom("r0");
        int idr0 = h.getRoom().get(0).getId();

        h.addEquipment("eq0","light",false,false,idr0);
        int ideq0 = edb.getLastEquipment();
        h.addEquipment("eq1","light",false,false,idr0);
        int ideq1 = edb.getLastEquipment();

        h.addNotification("turn off the light", ideq0);
        h.addNotification("turn on the light", ideq0);
        h.addNotification("turn on the light", ideq1);

        h.deleteNotification(h.getNotifs().get(0));
        assertEquals(h.getNotifs().size(), 2);
        assertEquals(ndb.getAllNotifs(h).size(),2);

    }
}