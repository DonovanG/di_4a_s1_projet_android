package com.androstock.myweatherapp.test;

import com.androstock.myweatherapp.model.Equipment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EquipmentTest {

    Equipment e;

    @Before
    public void setUp() {
        e = new Equipment(0, "eq0", "light", false, false);
    }

    @After
    public void tearDown() {
        e = null;
    }

    @Test
    public void getId() {
        assertEquals(e.getId(), 0);
    }

    @Test
    public void getName() {
        assertEquals(e.getName(), "eq0");

    }

    @Test
    public void setName() {
        assertEquals(e.getName(), "eq0");
        e.setName("eq1");
        assertEquals(e.getName(), "eq1");

    }

    @Test
    public void getCategory() {
        assertEquals(e.getCategory(), "light");

    }

    @Test
    public void isOutside() {
        assertEquals(e.isOutside(), false);

    }
}