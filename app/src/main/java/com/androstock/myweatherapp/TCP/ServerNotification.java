package com.androstock.myweatherapp.TCP;

import com.androstock.myweatherapp.controller.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ServerNotification implements Runnable {

    /** the TCP socket */
    Socket s;
    /** the reading stream */
    InputStream is;
    /** the pointing read */
    String msg;

    private byte[] buffers = new byte[1000];


    public ServerNotification(Socket s){
        this.s = s;

    }

    @Override
    public void run(){
        System.out.println("Server reading...");

        try {
            is = s.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            int n = is.read(buffers);
            msg = new String(buffers, 0, n);
            String[] tab = msg.split(";");
            MainActivity.model.addNotification(tab[0], Integer.parseInt(tab[1]));

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Server reading done ...\n\nThe msg is : "+msg);

        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

         }
}
