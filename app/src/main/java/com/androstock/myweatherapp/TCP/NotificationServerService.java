package com.androstock.myweatherapp.TCP;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.androstock.myweatherapp.model.Notification;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NotificationServerService extends AsyncTask {


    private ServerSocket ss;
    private InetSocketAddress isA;

    public NotificationServerService(String ipAddress, int port){

        isA = new InetSocketAddress(ipAddress,port);

        try {
            ss = new ServerSocket(isA.getPort());
        } catch (IOException e) {
            System.exit(0);
        }
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        System.out.println("TCP Server lunched");

        while(true){

            try {
                new Thread(new ServerNotification(ss.accept())).start();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
