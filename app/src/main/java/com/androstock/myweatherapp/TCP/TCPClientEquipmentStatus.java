package com.androstock.myweatherapp.TCP;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClientEquipmentStatus extends AsyncTask {

    private Socket s;
    private OutputStream oo;
    private int status;
    private int id;
    private static int TRAME_NUM = 1;
    private Context context;

    public TCPClientEquipmentStatus(String ip, int port, int status, int id, Context context){

        this.context = context;

        try {
            s = new Socket(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            Toast toast = Toast.makeText(context, "connexion failure : socket null", Toast.LENGTH_LONG);
            toast.show();

        }


        try {
            oo = s.getOutputStream();
        } catch (IOException e) {
            System.out.println("no output stream");
        } catch (NullPointerException e){
            Toast toast = Toast.makeText(context, "connexion failure : socket null", Toast.LENGTH_LONG);
            toast.show();

        }

        this.status = status;
        this.id = id;

    }


    @Override
    protected Object doInBackground(Object[] objects) {

        Log.d("client lunched", "client lunched");
        try {
            oo.write((TRAME_NUM+";"+status+";"+id).getBytes());
            Log.d("client send the msg...", "client send the msg...");
            s.close();
            oo.close();


        } catch(IOException e)
        {
            Log.d("waiting...", "waiting...");
        }

        return null;
    }
}
