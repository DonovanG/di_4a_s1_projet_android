package com.androstock.myweatherapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * class that manage the rooms
 */
public class Room implements Parcelable {

    /** attributes */
    private int id;
    private String name;
    private ArrayList<Equipment> equipments;

    /**
     * constructor
     * @param name
     */
    public Room(int id, String name) {
        this.id = id;
        this.name = name;
        equipments = new ArrayList<>();
    }

    /**
     * constructor that create a parcelable room that we can send between activities
     * @param in
     */
    protected Room(Parcel in) {
        name = in.readString();
    }

    /**
     * create a parcelable room
     */
    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel in) {
            return new Room(in);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

    /**
     * get the room id
     * @return the id
     */
    public final int getId() {
        return id;
    }

    /**
     * get the room name
     * @return the room name
     */
    public String getName() {
        return name;
    }

    /**
     * add an equipment to the room
     * @param e the equipment to add
     */
    public void addEquipment(Equipment e){
        equipments.add(e);
    }

    /**
     * get the list of equipments in the room
     * @return the list of equipments
     */
    public ArrayList<Equipment> getEquipments(){
        return equipments;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        for(Equipment e : equipments)
            dest.writeParcelable(e, flags);
    }

    /**
     * delete an equipment in the room
     * @param e the equipment to delete
     */
    public void deleteEquipment(Equipment e) {
        equipments.remove(e);
    }
}
