package com.androstock.myweatherapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * class that manage the notifications
 */
public class Notification implements Parcelable {
    /** attributes */
    private final int id;
    private String msg;
    private Equipment equipment;


    /**
     * constructor
     * @param msg
     * @param equipment
     */
    public Notification(int id, String msg, Equipment equipment) {
        this.id = id;
        this.msg = msg;
        this.equipment = equipment;
    }

    /**
     * constructor that create a parcelable notification that we can send between activities
     * @param in
     */
    protected Notification(Parcel in) {
        id = in.readInt();
        msg = in.readString();
        equipment = in.readParcelable(Equipment.class.getClassLoader());
    }

    /**
     * create the parcelable notification
     */
    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    /**
     * getter of the ID
     * @return the id
     */
    public final int getId() {
        return id;
    }

    /**
     * get the notification's message
     * @return the message
     */
    public String getMsg() {
        return msg;
    }

    /**
     * get the equipment linked to the notification
     * @return
     */
    public Equipment getEquipment() {
        return equipment;
    }



    @Override
    public String toString() {
        return msg+"\nEquipement : "+equipment.getName()+"\nCatégorie : "+equipment.getCategory();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(msg);
        dest.writeParcelable(equipment, flags);
    }


}
