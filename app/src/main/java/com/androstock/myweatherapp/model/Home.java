package com.androstock.myweatherapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.androstock.myweatherapp.controller.database.EquipmentDAO;
import com.androstock.myweatherapp.controller.database.NotificationDAO;
import com.androstock.myweatherapp.controller.database.RoomDAO;

import java.util.ArrayList;


/**
 * Class that represent the model, contain the rooms of the user and the notifications
 */
public class Home implements Parcelable {

    /** attributes */
    private ArrayList<Room> rooms;
    private ArrayList<Notification> notifs;

    /** object that manage the DB */
    RoomDAO rDAO;
    NotificationDAO nDAO;
    EquipmentDAO eDAO;

    /**
     * constructor
     * @param context
     */
    public Home(Context context, String dbname) {

        /** initialize the DB management */
        rDAO = new RoomDAO(context, dbname);
        nDAO = new NotificationDAO(context, dbname);
        eDAO = new EquipmentDAO(context, dbname);

        /** get objects from the DB */
        rooms = rDAO.getRooms();
        notifs = nDAO.getAllNotifs(this);
    }

    /**
     * constructor
     * @param rooms : list of test rooms
     */
    public Home(ArrayList<Room> rooms, ArrayList<Notification> notifs) {


        /** get objects from the DB */
        this.rooms = rooms;
        this.notifs = notifs;
    }



    //---------------PARCELABLE IMPLEMENTATION------------------------
    protected Home(Parcel in) {
    }

    public static final Creator<Home> CREATOR = new Creator<Home>() {
        @Override
        public Home createFromParcel(Parcel in) {
            return new Home(in);
        }

        @Override
        public Home[] newArray(int size) {
            return new Home[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        for(Room r : rooms)
            dest.writeParcelable(r, flags);
        for(Notification n : notifs)
            dest.writeParcelable(n, flags);
    }



    //------------------------ROOM----------------------------

    /**
     * getter of the room list attribute
     * @return the list of the rooms
     */
    public ArrayList<Room> getRoom() {
        return rooms;
    }

    /**
     * getter of one specific room by its ID
     * @param id
     * @return the room that we are looking for, null if it doesn't exist
     */
    public Room getRoomById(int id){
        for(Room r : rooms){
            if(r.getId() == id)
                return r;
        }

        return null;

    }

    /**
     * get a room that contain a specific equipment
     * @return the room or null if it doesn't exist
     */
    public Room getRoomByEquimentId(int idEquipment){

        for(Room r : rooms){
            for(Equipment e : r.getEquipments()){
                if(e.getId() == idEquipment)
                    return r;
            }

        }

        return null;

    }

    /**
     * add room to the model and to the DB
     * @param name
     */
    public void addRoom(String name){
        if(name != null) {
            rDAO.add(name);
            int id = rDAO.getLastRoomId();
            rooms.add(new Room(id, name));
        }
    }

    /** delete room from the model and the DB
     * @param r
     */
    public void deleteRoom(Room r){
        rooms.remove(r);
        rDAO.delete(r.getId());
    }

    //----------------------EQUIPMENT-----------------------

    /**
     * getter of the list of equipments
     * @return the list of equipments
     */
    public ArrayList<Equipment> getEquipments(){
        ArrayList<Equipment> equipments = new ArrayList<>();
        for(Room r : rooms){
            for (Equipment e : r.getEquipments()){
                equipments.add(e);
            }
        }
        return equipments;

    }

    /**
     * find an equipment by its ID
     * @param id
     * @return the equipment, null if it doesn't exist
     */
    public Equipment getEquipmentById(int id){
        for(Equipment e : getEquipments()){
            if(e.getId() == id)
                return e;
        }

        return null;

    }

    /**
     * get equipments of a specific room
     * @param r : the room
     * @return the list of equipments that contain the room,
     */
    public ArrayList<Equipment> getEquipments(Room r) {

        if(r == null)
            throw new IllegalArgumentException();

        ArrayList<Equipment> equipments = new ArrayList<>();
        for(Equipment e : r.getEquipments())
            equipments.add(e);
        return equipments;
    }

    /**
     * add an equipment in the model and the DB
     */
    public void addEquipment(String name, String cat, boolean isOutside, boolean state, int idRoom){
        Room r = null;
        Equipment e = null;

        if(name != null && cat!= null) {
            r = getRoomById(idRoom);
            if(r != null){
                eDAO.add(name,cat,isOutside,state,idRoom);
                int idEq = eDAO.getLastEquipment();
                e = new Equipment(idEq,name,cat,isOutside,state);
                r.addEquipment(e);
            }
        }
    }

    /**
     * delete an equipment from the model and the DB
     * @param e the equipment
     */
    public void deleteEquipment(Equipment e){
        if(e != null) {
            getRoomByEquimentId(e.getId()).deleteEquipment(e);
            eDAO.delete(e.getId());
        }
    }

    //---------------------------NOTIFICATION----------------------------

    /**
     * getter of the list of notifications
     * @return the list of notifications
     */
    public ArrayList<Notification> getNotifs() {
        return notifs;
    }

    /**
     * add a notification in the model and the DB
     */
    public void addNotification(String msg, int idEquipment){
        Notification n = null;
        Equipment e = null;

        if(msg != null) {
            e = getEquipmentById(idEquipment);
            if(e != null){
                nDAO.add(msg, idEquipment);
                int id = nDAO.getLastNotificationId();
                n = new Notification(id,msg,e);
                notifs.add(n);
            }
        }
    }

    /**
     * delete a notification from the model and the DB
     * @param n : the notification
     */
    public void deleteNotification(Notification n){
        if(n != null) {
            notifs.remove(n);
            nDAO.delete(n.getId());
        }
    }



}
