package com.androstock.myweatherapp.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class that manage the equipments of the user
 */
public class Equipment implements Parcelable {

    /** attributes */
    private final int id;

    private String name;

    private String category;

    private boolean outside;



    private boolean state;

    /**
     * constructor of an equipment
     * @param name
     * @param category
     * @param outside
     */
    public Equipment(int id, String name, String category, boolean outside, boolean state) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.outside = outside;
        this.state = state;
    }

    /**
     * Constructor for the parcelable equipment, allow to serialize it
     * @param in
     */
    protected Equipment(Parcel in) {
        id = in.readInt();
        name = in.readString();
        category = in.readString();
        outside = in.readByte() != 0;
    }

    /**
     * create a parcelable equipment that can be send between activities
     */
    public static final Creator<Equipment> CREATOR = new Creator<Equipment>() {
        @Override
        public Equipment createFromParcel(Parcel in) {
            return new Equipment(in);
        }

        @Override
        public Equipment[] newArray(int size) {
            return new Equipment[size];
        }
    };

    /**
     * getter of id attribute
     * @return id
     */
    public final int getId(){ return id;}

    /**
     * getter of name attribute
     * @return name
     */
    public String getName() {

        return name;
    }

    /**
     * setter of name attribute
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter of category attribute
     * @return category name
     */
    public String getCategory() {
        return category;
    }

    /** getter of isOutSide attibute
     *
     * @return is the equipment outside or not ?
     */
    public boolean isOutside() {
        return outside;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(category);
        dest.writeByte((byte) (outside ? 1 : 0));
    }
}
