package com.androstock.myweatherapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.controller.MainActivity;
import com.androstock.myweatherapp.model.Notification;

import java.util.List;

/**
 *  allow to create an equipment list in a listView
 */
public class NotificationListAdapter extends ArrayAdapter<Notification> {

    private int layout;

    /**
     * constructor
     * @param context
     * @param resource
     * @param notifs
     */
    public NotificationListAdapter(@NonNull Context context, int resource, @NonNull List<Notification> notifs) {
        super(context, resource, notifs);
        layout = resource;

    }

    /**
     * get the view for a position in the list
     * @param position
     * @param convertView
     * @param parent
     * @return the view
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        NotificationtHolder holder;

        if(convertView == null){

            holder = new NotificationtHolder();


            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(layout, null, true);

            holder.msgTxtView = convertView.findViewById(R.id.notificationMsgTxt);
            holder.eqTxtView = convertView.findViewById(R.id.notificationEquipTxt);

            convertView.setTag(holder);
        }
        else
            holder = (NotificationtHolder) convertView.getTag();

        Notification n = getItem(position);
        if(n != null){
            holder.msgTxtView.setText(n.getMsg());
            holder.eqTxtView.setText(n.getEquipment().getName()+", in Room : "+MainActivity.model.getRoomByEquimentId(n.getEquipment().getId()));

        }



        return convertView;
    }

    /**
     * implement the items of the view
     */
    static class  NotificationtHolder{
        TextView msgTxtView;
        TextView eqTxtView;
    }
}
