package com.androstock.myweatherapp.adapters;

import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.model.Equipment;

import java.util.List;

/**
 *  allow to create an equipment list in a listView
 */
public class EquipmentListAdapter extends ArrayAdapter<Equipment> {

    private int layout;

    /**
     * constructor
     * @param context
     * @param resource
     * @param equipments
     */
    public EquipmentListAdapter(@NonNull Context context, int resource, @NonNull List<Equipment> equipments) {
        super(context, resource, equipments);
        layout = resource;

    }

    /**
     * get the view for a position in the list
     * @param position
     * @param convertView
     * @param parent
     * @return the view
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        EquipmentHolder holder;

        if(convertView == null){

            holder = new EquipmentHolder();


            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(layout, null, true);

            holder.txtView = convertView.findViewById(R.id.equipment_name_text_view);
            holder.s = convertView.findViewById(R.id.equipment_switch);

            convertView.setTag(holder);
        }
        else
            holder = (EquipmentHolder) convertView.getTag();

        final Equipment e = getItem(position);
        if(e != null){
            holder.txtView.setText(e.toString());
            holder.s.setChecked(e.getState());
            holder.s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                        //new TCPClientEquipmentStatus("10.192.168.135",8083,1,e.getId(), getContext()).execute();

                        Toast toast = Toast.makeText(getContext(), "equipment turn on", Toast.LENGTH_LONG);
                        toast.show();

                    }
                        // send message to raspberry, turn on equipment
                        //trame example : type_trame;boolean;id_equipment
                    else {
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                        //new TCPClientEquipmentStatus("localhost",8083,0,e.getId(), getContext()).execute();

                        Toast toast = Toast.makeText(getContext(), "equipment turn off", Toast.LENGTH_LONG);
                        toast.show();

                    }

                }
            });
        }



        return convertView;
    }

    /**
     * implement the items of the view
     */
    static class  EquipmentHolder{
        TextView txtView;
        Switch s;
    }
}
