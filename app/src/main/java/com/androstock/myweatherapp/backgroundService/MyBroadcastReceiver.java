package  com.androstock.myweatherapp.backgroundService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;


public class MyBroadcastReceiver extends BroadcastReceiver {
    private static String TAG = "Prog - BC";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "before start");


        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

// Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int interval = 60_000;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Log.d(TAG, "manager set up ?" + manager.equals(null) );
        Log.d(TAG, "alarm set");

    }

}
