package com.androstock.myweatherapp.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/**
 * controller that manage the parameter view
 */
public class ProgActivity extends AppCompatActivity {

    private Spinner roomSpinner;
    private Spinner catSpinner;
    private Spinner eqSpinner;
    private Switch repSwitch;
    private CheckBox dayChkBox;
    private CheckBox weekChkBox;
    private CheckBox monthChkBox;
    private CheckBox yearBox;
    private EditText dateEditTxt;
    private EditText hourEditTxt;
    private Button okBtn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prog);

        roomSpinner = findViewById(R.id.room_spinner_prog_activity);
        ArrayAdapter<Room> roomAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        roomAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomSpinner.setAdapter(roomAdapter);

        catSpinner = findViewById(R.id.category_spinner_prog_activity);
        ArrayAdapter<String> catSpinnerAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, MainActivity.CATEGORIES);
        catSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catSpinner.setAdapter(catSpinnerAdapter);

        roomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Equipment> equipments = new ArrayList<>();
                eqSpinner = findViewById(R.id.eq_spinner_prog_activity);
                ArrayList<Equipment>  temp = MainActivity.model.getRoomById((
                        (Room)(roomSpinner.getSelectedItem())).getId()).getEquipments();

                for(Equipment e : temp){
                    if(e.getCategory().equals(catSpinner.getSelectedItem()))
                        equipments.add(e);
                }


                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(ProgActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eqSpinner.setAdapter(equipmentSpinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ArrayList<Equipment> equipments = new ArrayList();
                eqSpinner = findViewById(R.id.eq_spinner_prog_activity);
                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(ProgActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eqSpinner.setAdapter(equipmentSpinnerAdapter);
            }
        });


        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Equipment> equipments = new ArrayList<>();
                eqSpinner = findViewById(R.id.eq_spinner_prog_activity);
                ArrayList<Equipment>  temp = MainActivity.model.getRoomById((
                        (Room)(roomSpinner.getSelectedItem())).getId()).getEquipments();

                for(Equipment e : temp){
                    if(e.getCategory().equals(catSpinner.getSelectedItem()))
                        equipments.add(e);
                }


                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(ProgActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eqSpinner.setAdapter(equipmentSpinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ArrayList<Equipment> equipments = new ArrayList();
                eqSpinner = findViewById(R.id.eq_spinner_prog_activity);
                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(ProgActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eqSpinner.setAdapter(equipmentSpinnerAdapter);
            }
        });


        repSwitch = findViewById(R.id.repetition_switch_prog_activity);
        repSwitch.setChecked(true);
        repSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked) {
                    dayChkBox.setEnabled(false);
                    weekChkBox.setEnabled(false);
                    monthChkBox.setEnabled(false);
                    yearBox.setEnabled(false);
                }
                else{
                    dayChkBox.setEnabled(true);
                    weekChkBox.setEnabled(true);
                    monthChkBox.setEnabled(true);
                    yearBox.setEnabled(true);
                }

            }
        });

        dayChkBox = findViewById(R.id.day_chkBox_prog_activity);


        weekChkBox = findViewById(R.id.week_chkBox_prog_activity);

        monthChkBox = findViewById(R.id.month_chkBox_prog_activity);

        yearBox = findViewById(R.id.year_chkBox_prog_activity);

        dateEditTxt = findViewById(R.id.date_editTxt_prog_activity);

        hourEditTxt = findViewById(R.id.hour_editTxt_prog_activity);

        okBtn = findViewById(R.id.ok_btn_prog_activity);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send prog to raspberry
            }
        });

    }


}
