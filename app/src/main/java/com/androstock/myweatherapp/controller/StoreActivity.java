package com.androstock.myweatherapp.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.adapters.EquipmentListAdapter;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/**
 * controller that manage the blinders view
 */
public class StoreActivity extends AppCompatActivity {

    /** atributes */
    Spinner spinner;
    ListView storeList;
    ArrayList<Equipment> stores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        /** spinner that allow to choose the room where we want to see light's states */
        spinner = findViewById(R.id.roomSpinnerStoreActivity);
        ArrayAdapter<Room> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        storeList = findViewById(R.id.listStoreActivity);

        /** when we select a room, we update the list of equipment */
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stores = new ArrayList<>();
                if(spinner.getSelectedItem() != null) {
                    for (Equipment e : MainActivity.model.getEquipments((Room) spinner.getSelectedItem())) {
                        if (e.getCategory().equals(MainActivity.CATEGORIES[1])) {
                            stores.add(e);
                        }
                    }
                }

                storeList.setAdapter(new EquipmentListAdapter(StoreActivity.this, R.layout.list_equipment, stores));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                stores = new ArrayList<>();

                storeList.setAdapter(new EquipmentListAdapter(StoreActivity.this, R.layout.list_equipment, stores));
            }
        });

    }
}
