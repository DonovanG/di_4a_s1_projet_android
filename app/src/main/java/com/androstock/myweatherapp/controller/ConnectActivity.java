package com.androstock.myweatherapp.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.TCP.NotificationServerService;

/**
 * controller for the first activity that appear, ask user to connect
 */
public class ConnectActivity extends AppCompatActivity {

    public static SharedPreferences preferences;


    /** attributes */
    Button button;
    EditText logInText;
    EditText pwdText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        preferences = getPreferences(MODE_PRIVATE);


        String login = preferences.getString("login", "raté");

        /** start server for notifications */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        new NotificationServerService("localhost",8084).execute();

        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        logInText = findViewById(R.id.logInEditTxt);
        pwdText = findViewById(R.id.pwdEditTxt);

        /** button that validate the information that the user give */
        button = findViewById(R.id.connectButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(preferences.getString("login", null) != null){
                    if(logInText.getText().toString().equals(preferences.getString("login", null))
                            && pwdText.getText().toString().equals(preferences.getString("pwd", null))) {
                        Intent mainActivityIntent = new Intent(ConnectActivity.this, MainActivity.class);
                        startActivity(mainActivityIntent);
                    }
                    else{
                        pwdText.setTextColor(Color.rgb(255,0,0));
                        pwdText.setText("try again");
                    }
                }
                else{
                    preferences.edit().putString("login", logInText.getText().toString()).apply();
                    preferences.edit().putString("pwd", pwdText.getText().toString()).apply();

                    Intent mainActivityIntent = new Intent(ConnectActivity.this, MainActivity.class);
                    startActivity(mainActivityIntent);



                }
            }
        });
    }
}
