package com.androstock.myweatherapp.controller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/**
 * class that manage the room table of the DBs
 */
public class RoomDAO extends DAODB {

    /**
     * attributes
     */
    public static final String TABLE_NAME = "ROOM";
    public static final String KEY = "idRoom";
    public static final String NAME = "roomName";

    /**
     * constructor
     * @param pContext
     */
    public RoomDAO(Context pContext, String dbname) {
        super(pContext, dbname);
    }


    /**
     * add a row in the table
     * @param name : room name
     */
    public void add(String name) {
        open();
        ContentValues value = new ContentValues();
        value.put(RoomDAO.NAME, name);
        mDb.insert(RoomDAO.TABLE_NAME, null, value);
        close();
    }


    /**
     * delete a room from the table by its ID
     * @param id
     */
    public void delete(int id) {
        open();
        mDb.delete(TABLE_NAME, KEY + " = ?", new String[] {String.valueOf(id)});
        close();
    }

    /**
     * update a room in the table
     * @param r : a room object
     */
    public void update(Room r) {
        open();
        ContentValues value = new ContentValues();
        value.put(RoomDAO.NAME, r.getName());
        mDb.update(TABLE_NAME, value, KEY  + " = ?", new String[] {String.valueOf(r.getId())});
        close();
    }

    /**
     * get all the room object from the DB
     * @return
     */
    public ArrayList<Room> getRooms() {

        open();
        int i = 0;
        /** get all the row from the table room */
        Cursor c1 = mDb.rawQuery("select * from "+ RoomDAO.TABLE_NAME, null);
        Cursor c2;
        ArrayList<Room> rooms = new ArrayList<>();

        /** for all the rows get the equipment linked to it */
        while (c1.moveToNext()) {
            /** create the room object */
            rooms.add(new Room(c1.getInt(0), c1.getString(1)));
            /** get the equipments linked to the room in the table Equipment */
            c2 = mDb.rawQuery("select * " +
                                    "from EQUIPMENT " +
                                    "where idRoom = "+c1.getInt(0),
                            null);
            /** for each equipment found, create an object and add it to the room object */
            while(c2.moveToNext()){
                rooms.get(i).addEquipment(new Equipment(c2.getInt(0), c2.getString(1), c2.getString(2), c2.getInt(3) > 0, c2.getInt(4)>0));
            }
            c2.close();

            i++;
        }
        c1.close();
        close();

        return rooms;
    }

    /**
     * get the id of the last room added to the sql table,
     * allow to create a model that has the correct ids.
     * @return the id of the last room added
     */
    public int getLastRoomId() {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select max(idRoom) from "+ TABLE_NAME, null);
        int id = -1;

        /** for each equipment create an object */
        while (c.moveToNext()) {
            id = c.getInt(0);
        }
        c.close();
        close();

        if(id!=-1)
            return id;
        else
            throw new NullPointerException();
    }
}
