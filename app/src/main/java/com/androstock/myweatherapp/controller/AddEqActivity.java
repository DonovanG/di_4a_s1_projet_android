package com.androstock.myweatherapp.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.controller.database.EquipmentDAO;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/**
 * controller for the the activity that manage equipments
 */
public class AddEqActivity extends AppCompatActivity {

    // Le nom du fichier qui représente ma base
    protected final static String DBNAME = "databaseV2.db";

    /** attributes */
    Button roomButton;
    Button addEqButton;
    Button delEqButton;
    EditText nameEditText;
    Spinner addRoomSpinner;
    Spinner catSpinner;
    Spinner deleteRoomSpinner;
    Spinner equipmentSpinner;

    EquipmentDAO db = new EquipmentDAO(this, DBNAME);

    /**
     * creating the view for adding an equipment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** get the equipment adding activity*/
        setContentView(R.layout.activity_add_equipment);

        /** edit text for the equipment name */
        nameEditText = findViewById(R.id.name_edit_text_equipment_activity);

        /** spinner to choose the room that contain the equipment */
        addRoomSpinner = findViewById(R.id.room_spinner_add_equipment_activity);
        ArrayAdapter<Room> addRoomSpinnerAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        addRoomSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addRoomSpinner.setAdapter(addRoomSpinnerAdapter);

        /** spinner to choose the category of the equipment (light, blinder, thermometer...)*/
        catSpinner = findViewById(R.id.category_spinner_equipment_activity);
        ArrayAdapter<String> catSpinnerAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, MainActivity.CATEGORIES);
        catSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catSpinner.setAdapter(catSpinnerAdapter);

        /** spinner to choose the equipment that we want to delete */
        deleteRoomSpinner = findViewById(R.id.room_spinner_delete_equipment_activity);
        ArrayAdapter<Room> delRoomSpinnerAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        delRoomSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deleteRoomSpinner.setAdapter(delRoomSpinnerAdapter);
        deleteRoomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Equipment> equipments;
                equipmentSpinner = findViewById(R.id.equipment_spinner_equipment_activity);
                equipments = MainActivity.model.getRoomById((
                        (Room)(deleteRoomSpinner.getSelectedItem())).getId()).getEquipments();
                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(AddEqActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                equipmentSpinner.setAdapter(equipmentSpinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ArrayList<Equipment> equipments = new ArrayList();
                equipmentSpinner = findViewById(R.id.equipment_spinner_equipment_activity);
                ArrayAdapter equipmentSpinnerAdapter = new ArrayAdapter(AddEqActivity.this,
                        android.R.layout.simple_spinner_item, equipments);
                equipmentSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                equipmentSpinner.setAdapter(equipmentSpinnerAdapter);
            }
        });


        /** button to go on the adding room activity*/
        roomButton = findViewById(R.id.add_room_button);
        roomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddEqActivity.this, AddRoomActivity.class );
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        /** button to validate the equipment to add*/
        addEqButton = findViewById(R.id.add_button_equipment_activity);
        addEqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!nameEditText.equals("") && addRoomSpinner.getSelectedItem()!=null &&
                        catSpinner.getSelectedItem()!=null ){


                    MainActivity.model.addEquipment(
                            nameEditText.getText().toString(),
                            catSpinner.getSelectedItem().toString(),
                            false,
                            false,
                            ((Room) addRoomSpinner.getSelectedItem()).getId());
                }

            }
        });

        /** button to validate the equipment to delete */
        delEqButton = findViewById(R.id.delete_button_equipment_activity);
        delEqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deleteRoomSpinner.getSelectedItem() != null &&
                        equipmentSpinner.getSelectedItem() != null){
                    MainActivity.model.deleteEquipment((Equipment) equipmentSpinner.getSelectedItem());
                }
            }
        });
    }

    /**
     * go back to the main menu when we click on the "back button"
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(AddEqActivity.this, AddRoomActivity.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);        }

            return true;

    }

}
