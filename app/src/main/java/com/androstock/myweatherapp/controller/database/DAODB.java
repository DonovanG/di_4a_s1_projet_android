package com.androstock.myweatherapp.controller.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * abstract class that create the database, allow to open it and close it
 */
public abstract class DAODB {
    /** database version that you have to change if the DB change */
    protected final static int VERSION = 2;


    /** attributes */
    protected SQLiteDatabase mDb = null;
    protected DBHandler mHandler = null;


    /**
     * constructor
     * @param pContext
     */
    public DAODB(Context pContext, String dbname) {

        this.mHandler = new DBHandler(pContext, dbname, null, VERSION);

    }


    /**
     * open the database by initializing the SQLiteDatabase attribute
     * @throws SQLException
     */
    public void open() throws SQLException {

        mDb = mHandler.getWritableDatabase();

    }


    /**
     * close the database
     */
    public void close() {

        
        mDb.close();

    }

}
