package com.androstock.myweatherapp.controller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.androstock.myweatherapp.model.Equipment;

import java.util.ArrayList;

/**
 * class that manage the Equipment table of the DataBase
 */
public class EquipmentDAO extends DAODB{

    /** attributes */
    private static final String TABLE_NAME = "EQUIPMENT";
    private static final String KEY = "idEquipment";
    private static final String NAME = "equipmentName";
    private static final String ROOM = "idRoom";
    private static final String CATEGORY = "category";
    private static final String OUTSIDE = "outside";
    private static final String STATE = "state";



    /**
     * constructor
     * @param pContext
     */
    public EquipmentDAO(Context pContext, String dbname) {
        super(pContext, dbname);
    }

       /**
     * add with attributes
     * @param name
     * @param category
     * @param outside
     * @param state
     * @param idRoom
     */
    public void add(String name, String category, boolean outside, boolean state, int idRoom) {
        open();
        ContentValues value = new ContentValues();
        value.put(EquipmentDAO.NAME, name);
        value.put(EquipmentDAO.CATEGORY, category);
        value.put(EquipmentDAO.OUTSIDE, outside);
        value.put(EquipmentDAO.STATE, state);
        value.put(EquipmentDAO.ROOM, idRoom);
        mDb.insert(EquipmentDAO.TABLE_NAME, null, value);
        close();
    }

    /**
     * delete a row from the table by its ID
     * @param id
     */
    public void delete(int id) {
        open();
        mDb.delete(TABLE_NAME, KEY + " = ?", new String[] {String.valueOf(id)});
        close();
    }

    /**
     * update a row in the table
     * @param e
     * @param idRoom
     */
    public void update(Equipment e, int idRoom) {
        open();
        ContentValues value = new ContentValues();
        value.put(EquipmentDAO.NAME, e.getName());
        value.put(EquipmentDAO.ROOM, idRoom);
        value.put(EquipmentDAO.CATEGORY, e.getCategory());
        value.put(EquipmentDAO.OUTSIDE, e.isOutside());
        mDb.update(TABLE_NAME, value, KEY  + " = ?", new String[] {String.valueOf(e.getId())});
        close();
    }

    /**
     * get the equipments object from the DB
     * @return the list of equipments objects
     */
    public ArrayList<Equipment> getAllEquipments() {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select * from "+ TABLE_NAME, null);
        ArrayList<Equipment> equipments = new ArrayList<>();

        /** for each equipment create an object */
        while (c.moveToNext()) {
                Equipment e = new Equipment(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3) > 0, c.getInt(4) > 0);
                equipments.add(e);
        }
        c.close();
    close();
        return equipments;
    }

    /**
     * get the equipment object from the DB
     * @return the equipment objects
     */
    public Equipment getEquipment(int id) {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select * from "+ TABLE_NAME +" where "+ KEY +" = "+ id, null);
        Equipment e = null;

        /** for each equipment create an object */
        while (c.moveToNext()) {
            e = new Equipment(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3) > 0, c.getInt(4) > 0);
        }
        c.close();
        close();
        return e;
    }

    /**
     * get the equipment object from the DB
     * @return the equipment objects
     */
    public Equipment getEquipmentI(int position) {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select * from "+ TABLE_NAME, null);
        Equipment e = null;
        int count = 0;

        /** for each equipment create an object */
        while (c.moveToNext()) {
            if(count == position-1)
                e = new Equipment(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3) > 0, c.getInt(4) > 0);
            count++;
        }
        c.close();
        close();
        return e;
    }

    /**
     * get the id of the last equipment add to the sql table,
     * allow to create a model that has the correct ids.
     * @return the id of the last equipment added
     */
    public int getLastEquipment() {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select max(idEquipment) from "+ TABLE_NAME, null);
        int id = -1;

        /** for each equipment create an object */
        while (c.moveToNext()) {
            id = c.getInt(0);
        }
        c.close();
        close();

        if(id!=-1)
            return id;
        else
            throw new NullPointerException();
    }
}
