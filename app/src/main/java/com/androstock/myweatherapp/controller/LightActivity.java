package com.androstock.myweatherapp.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.adapters.EquipmentListAdapter;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/**
 * controller of the activity that mange the lights
 */
public class LightActivity extends AppCompatActivity {

    /** atributes */
    Spinner spinner;
    ListView lightList;
    ArrayList<Equipment> lights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);

        /** spinner that allow to choose the room where we want to see light's states */
        spinner = findViewById(R.id.roomSpinnerLightActivity);
        ArrayAdapter<Room> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        lightList = findViewById(R.id.light_list);

        /** when we select a room, we update the list of equipment */
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                lights = new ArrayList<>();
                if(spinner.getSelectedItem() != null) {
                    for (Equipment e : MainActivity.model.getEquipments((Room) spinner.getSelectedItem())) {
                        if (e.getCategory().equals(MainActivity.CATEGORIES[0])) {
                            lights.add(e);
                        }
                    }
                }

                //lightList.setAdapter(new ArrayAdapter<>(LightActivity.this, android.R.layout.simple_list_item_1, lights ));

                lightList.setAdapter(new EquipmentListAdapter(LightActivity.this, R.layout.list_equipment, lights));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                lights = new ArrayList<>();
                //lightList.setAdapter(new ArrayAdapter<>(LightActivity.this, android.R.layout.simple_list_item_1, lights ));

                lightList.setAdapter(new EquipmentListAdapter(LightActivity.this, R.layout.list_equipment, lights));
            }
        });

    }
}
