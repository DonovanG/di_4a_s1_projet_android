package com.androstock.myweatherapp.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.adapters.NotificationListAdapter;
import com.androstock.myweatherapp.model.Notification;

/**
 * controller that manage the notification view
 */
public class NotifActivity extends AppCompatActivity {

    /** list of notifications */
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        /** set the notification list */
        list = findViewById(R.id.notifListView);
        list.setAdapter(new NotificationListAdapter(NotifActivity.this,R.layout.list_notification,MainActivity.model.getNotifs()));
        list.setClickable(true);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Notification n = (Notification) list.getItemAtPosition(position);
                String category = n.getEquipment().getCategory();

                Intent intent = null;


                switch(category){
                    case "light" :
                        intent = new Intent(NotifActivity.this, LightActivity.class );
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case "blinder" :
                        intent = new Intent(NotifActivity.this, StoreActivity.class );
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case "thermometer" :
                        intent = new Intent(NotifActivity.this, ThermometerActivity.class );
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
