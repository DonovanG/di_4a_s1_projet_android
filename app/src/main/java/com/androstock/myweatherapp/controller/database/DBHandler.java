package com.androstock.myweatherapp.controller.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

/**
 * Class that allow to initialize the database by creating or updating tables
 */
public class DBHandler extends SQLiteOpenHelper {

    /**
     * constructor
     * @param context : the activity that is use for creating the database
     * @param name : the database name
     * @param factory : null
     * @param version : the DB version
     */
    public DBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * create tables on the initialization of the DB
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        //create table ROOM
        db.execSQL("CREATE TABLE ROOM ("+
                "idRoom INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "roomName TEXT);"
        );


        //create table EQUIPMENT
        db.execSQL("CREATE TABLE EQUIPMENT ("+
                "idEquipment INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "equipmentName TEXT, "+
                "category TEXT, "+
                "outside BOOLEAN, "+
                "state BOOLEAN, "+
                "idRoom INTEGER, "+
                "FOREIGN KEY(idRoom) REFERENCES ROOM(idRoom) ON DELETE CASCADE"+
                ");"
        );


        //create table NOTIFICATION
        db.execSQL("CREATE TABLE NOTIFICATION ("+
                    "idNotification INTEGER PRIMARY KEY AUTOINCREMENT, "+
                    "message TEXT, "+
                    "idEquipment INTEGER, "+
                    "FOREIGN KEY(idEquipment) REFERENCES EQUIPMENT(idEquipment) ON DELETE CASCADE"+
                    ");"
        );


    }

    /**
     * drop the tables if they already exist and create them
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS ROOM;");
        db.execSQL("DROP TABLE IF EXISTS EQUIPMENT;");
        db.execSQL("DROP TABLE IF EXISTS NOTIFICATION;");
        onCreate(db);

    }


}

