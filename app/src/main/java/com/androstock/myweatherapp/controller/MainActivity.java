package com.androstock.myweatherapp.controller;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.model.Function;
import com.androstock.myweatherapp.model.Home;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * controller for the Home Activity that allow the user to navigate through the activities
 */
public class MainActivity extends AppCompatActivity{

    // database name
    protected final static String DBNAME = "databaseV3.db";

    //MODEL
    public static Home model;
    public static final String[] CATEGORIES = {"light", "blinder", "thermometer"};


    /** attributes init */
    private TextView selectCity, cityField, currentTemperatureField, humidity_field, weatherIcon, updatedField;
    private ProgressBar loader;
    private Typeface weatherFont;
    private String city;
    private String OPEN_WEATHER_MAP_API = "88250bb90210a52743fd066505ea0668";

    private ImageButton addButton;
    private ImageButton lightButton;
    private ImageButton blinderButton;
    private ImageButton notifButton;
    private ImageButton thermButton;
    private ImageButton paramButton;

    @Override
    protected void onResume() {
        super.onResume();
        city = ConnectActivity.preferences.getString("city", "Tours, FR");
        loadWeather(city);
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        /** model instantiation */
        model = new Home(this, DBNAME);
        //model.addNotification("It's night, turn off the light", model.getEquipments().get(0).getId());
        //model.addNotification("It's night, turn off the light", model.getEquipments().get(1).getId());


        /** attributes & views instanciation */
        loader = findViewById(R.id.loader);
        selectCity = findViewById(R.id.selectCity);
        cityField = findViewById(R.id.city_field);
        updatedField = findViewById(R.id.updated_field);
        currentTemperatureField = findViewById(R.id.current_temperature_field);
        humidity_field = findViewById(R.id.humidity_field);
        weatherIcon = findViewById(R.id.weather_icon);
        weatherFont = Typeface.createFromAsset(getAssets(), "fonts/weathericons-regular-webfont.ttf");
        weatherIcon.setTypeface(weatherFont);

        /** button to go on the adding room activity */
        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, AddRoomActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        /** button to go on the lights activity */
        lightButton = findViewById(R.id.light_button);
        lightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, LightActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        /** button to go on the blinders activity */
        blinderButton = findViewById(R.id.blinder_button);
        blinderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, StoreActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        /** button to go on the notifications activity */
        notifButton = findViewById(R.id.notif_button);
        notifButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, NotifActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        /** button to go on the thermometers activity */
        thermButton = findViewById(R.id.thermometer_button);
        thermButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, ThermometerActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        /** button to go on the settings activity */
        paramButton = findViewById(R.id.param_button);
        paramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mainActivityIntent = new Intent(MainActivity.this, ParamActivity.class);
                startActivity(mainActivityIntent);
            }
        });

        city = ConnectActivity.preferences.getString("city", "Tours, FR");
        loadWeather(city);

        /**
         * button in order to change the city that manage the weather view
         */
        selectCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Change City");
                final EditText input = new EditText(MainActivity.this);
                input.setText(city);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Change",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                city = input.getText().toString();
                                loadWeather(city);
                            }
                        });
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialog.show();

                ConnectActivity.preferences.edit().putString("city", input.getText().toString()).apply();

                loadWeather(input.getText().toString());
            }
        });


    }

    public void loadWeather(String query) {
        if (Function.isNetworkAvailable(getApplicationContext())) {
            String xml = Function.excuteGet("http://api.openweathermap.org/data/2.5/weather?q=" + query +
                    "&units=metric&appid=" + OPEN_WEATHER_MAP_API);

            try {
                JSONObject json = new JSONObject(xml);
                if (json != null) {
                    JSONObject details = json.getJSONArray("weather").getJSONObject(0);
                    JSONObject main = json.getJSONObject("main");
                    DateFormat df = DateFormat.getDateTimeInstance();

                    cityField.setText(json.getString("name").toUpperCase(Locale.US) + ", " + json.getJSONObject("sys").getString("country"));
                    currentTemperatureField.setText(String.format("%.2f", main.getDouble("temp")) + "°");
                    humidity_field.setText("Humidity: " + main.getString("humidity") + "%");
                    updatedField.setText(df.format(new Date(json.getLong("dt") * 1000)));
                    weatherIcon.setText(Html.fromHtml(Function.setWeatherIcon(details.getInt("id"),
                            json.getJSONObject("sys").getLong("sunrise") * 1000,
                            json.getJSONObject("sys").getLong("sunset") * 1000)));

                    loader.setVisibility(View.GONE);

                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), "Error, Check City", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }




}