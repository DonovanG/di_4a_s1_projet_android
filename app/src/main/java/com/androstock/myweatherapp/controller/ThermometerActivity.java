package com.androstock.myweatherapp.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.adapters.EquipmentListAdapter;
import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Room;

import java.util.ArrayList;

/** controller that manage the thermometers view
 *
 */
public class ThermometerActivity extends AppCompatActivity {

    /** atributes */
    Spinner spinner;
    ListView thermList;
    ArrayList<Equipment> therm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thermometer);

        /** spinner that allow to choose the room where we want to see light's states */
        spinner = findViewById(R.id.roomSpinnerThermActivity);
        ArrayAdapter<Room> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        thermList = findViewById(R.id.thermList);

        /** when we select a room, we update the list of equipment */
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                therm = new ArrayList<>();
                if(spinner.getSelectedItem() != null) {
                    for (Equipment e : MainActivity.model.getEquipments((Room) spinner.getSelectedItem())) {
                        if (e.getCategory().equals(MainActivity.CATEGORIES[2])) {
                            therm.add(e);
                        }
                    }
                }

                thermList.setAdapter(new ArrayAdapter<>(ThermometerActivity.this, android.R.layout.simple_list_item_1, therm ));

                thermList.setAdapter(new EquipmentListAdapter(ThermometerActivity.this, R.layout.list_equipment, therm));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                therm = new ArrayList<>();

                thermList.setAdapter(new EquipmentListAdapter(ThermometerActivity.this, R.layout.list_equipment, therm));
            }
        });
    }
}
