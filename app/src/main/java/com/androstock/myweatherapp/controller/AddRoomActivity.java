package com.androstock.myweatherapp.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.androstock.myweatherapp.R;
import com.androstock.myweatherapp.model.Room;

/**
 * controller of the activity that manage the rooms
 */
public class AddRoomActivity extends AppCompatActivity {



    /** attributes */
    Button eqButton;
    Button addButton;
    Button delButton;
    EditText addRoomEditText;
    Spinner spinner;

    /**
     * creating the view for adding a room
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the activity */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        /** spinner that manage rooms */
        spinner = findViewById(R.id.roomSpinnerAddRoomActivity);
        ArrayAdapter<Room> adapter = new ArrayAdapter(
                this, android.R.layout.simple_spinner_item, MainActivity.model.getRoom());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        /** edit text to choose the room's name */
        addRoomEditText = findViewById(R.id.add_room_edit_text);

        /** button to go on the adding equipment activity */
        eqButton = findViewById(R.id.equipment_button);
        eqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddRoomActivity.this, AddEqActivity.class );
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        /** button to validate the room to add */
        addButton = findViewById(R.id.ok_add_room_button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String roomName = addRoomEditText.getText().toString();

                if(roomName != null)
                    MainActivity.model.addRoom(roomName);

                addRoomEditText.setText("");

            }
        });

        /** button to validate the room to delete */
        delButton = findViewById(R.id.ok_delete_room_button);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinner.getSelectedItem() != null)
                    MainActivity.model.deleteRoom(MainActivity.model.getRoomById(((Room)(spinner.getSelectedItem())).getId()));
            }
        });








    }

    /**
     * go back to the main menu when we click on the "back button"
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent mainActivityIntent = new Intent(AddRoomActivity.this, MainActivity.class);
            startActivity(mainActivityIntent);       }

        return true;

    }
}
