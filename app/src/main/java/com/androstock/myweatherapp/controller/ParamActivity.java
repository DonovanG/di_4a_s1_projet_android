package com.androstock.myweatherapp.controller;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.androstock.myweatherapp.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * controller that manage the parameter view
 */
public class ParamActivity extends AppCompatActivity {

    /** attributes */
    EditText loginEdtTxt;
    EditText pswdEdtTxt;
    EditText localisationEdtTxt;

    Button loginBtn;
    Button pswdBtn;
    Button localisationBtn;
    Button langageBtn;
    ImageButton progBtn;

    Spinner langageSpinner;


    /**
     * create the view for changing the parameters of the app
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /** get the view */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_param);

        /** chnage the log in */
        loginEdtTxt = findViewById(R.id.logInParamEditTxt);
        loginEdtTxt.setText(ConnectActivity.preferences.getString("login",""));

        loginBtn = findViewById(R.id.logInParamBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!loginEdtTxt.getText().equals("")){
                    ConnectActivity.preferences.edit().putString("login",loginEdtTxt.getText().toString()).apply();
                    Toast t = Toast.makeText(ParamActivity.this, "Log In changed", Toast.LENGTH_LONG);
                    t.show();
                }
            }
        });

        /** change the password */
        pswdEdtTxt  = findViewById(R.id.pswdParamEdtTxt);
        pswdEdtTxt.setText(ConnectActivity.preferences.getString("pwd",""));

        pswdBtn = findViewById(R.id.pswdParamBtn);
        pswdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!pswdBtn.getText().equals("")){
                    ConnectActivity.preferences.edit().putString("pwd",pswdEdtTxt.getText().toString()).apply();
                    Toast t = Toast.makeText(ParamActivity.this, "password changed", Toast.LENGTH_LONG);
                    t.show();
                }

            }
        });

        /** change the localisation for the weather*/
        localisationEdtTxt  = findViewById(R.id.localisationParamEdtTxt);
        localisationEdtTxt.setText(ConnectActivity.preferences.getString("city",null));

        localisationBtn = findViewById(R.id.localisationParamBtn);
        localisationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(localisationEdtTxt != null && !localisationEdtTxt.getText().toString().equals(""))
                    ConnectActivity.preferences.edit().putString("city", localisationEdtTxt.getText().toString()).apply();
                Toast t = Toast.makeText(ParamActivity.this, "city changed", Toast.LENGTH_LONG);
                t.show();
            }
        });

        /** change the language of the app */
        langageSpinner = findViewById(R.id.langageSpinner);
        ArrayList<String> languages = new ArrayList<>();
        languages.add("English");
        languages.add("French");
        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, languages);
        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        langageSpinner.setAdapter(SpinnerAdapter);

        langageBtn = findViewById(R.id.langageParamBtn);
        langageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale locale = new Locale((String) langageSpinner.getSelectedItem());
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
            }
        });

        /** go to the programing activity */
        progBtn = findViewById(R.id.prog_img_button);
        progBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ParamActivity.this, ProgActivity.class );
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });


    }
}
