package com.androstock.myweatherapp.controller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.androstock.myweatherapp.model.Equipment;
import com.androstock.myweatherapp.model.Home;
import com.androstock.myweatherapp.model.Notification;

import java.util.ArrayList;

/**
 * Class that manage the Notification table of the DB
 */
public class NotificationDAO extends DAODB {

    /** attributes */
    private static final String TABLE_NAME = "NOTIFICATION";
    private static final String KEY = "idNotification";
    private static final String MESSAGE = "message";
    private static final String EQUIPMENT = "idEquipment";

    /**
     * constructor
     * @param pContext
     */
    public NotificationDAO(Context pContext, String dbname) {
        super(pContext, dbname);
    }

    /** add a row in the notification table
     * @param msg : the notification message
     * @param idEquipment : id of the equipment for which the notification was commited
     */
    public void add(String msg, int idEquipment) {
        open();
        ContentValues value = new ContentValues();
        value.put(NotificationDAO.MESSAGE, msg);
        value.put(NotificationDAO.EQUIPMENT, idEquipment);
        mDb.insert(NotificationDAO.TABLE_NAME, null, value);
        close();
    }

    /**
     * delete a row from the table by its ID
     * @param id
     */
    public void delete(int id) {
        open();
        mDb.delete(TABLE_NAME, KEY + " = ?", new String[] {String.valueOf(id)});
        close();
    }


    /**
     * update a row in the table
     * @param n : the notification object
     */
    public void update(Notification n) {
        open();
        ContentValues value = new ContentValues();
        value.put(NotificationDAO.MESSAGE, n.getMsg());
        value.put(NotificationDAO.EQUIPMENT, n.getEquipment().getId());
        mDb.update(TABLE_NAME, value, KEY  + " = ?", new String[] {String.valueOf(n.getId())});
        close();
    }

    /**
     * get notifications object from the table
     * @param home
     * @return the list of all notifications
     */
    public ArrayList<Notification> getAllNotifs(Home home) {
        open();
            /** get all notifications from the DB */
            Cursor c = mDb.rawQuery("select * from "+ TABLE_NAME, null);
            ArrayList<Notification> notifs = new ArrayList<>();

            /** for each notification, create an object */
            while (c.moveToNext()) {
                Equipment e = home.getEquipmentById(c.getInt(2));

                notifs.add(new Notification(c.getInt(0), c.getString(1), e));
            }
            c.close();
        close();

        return notifs;

    }


    /**
     * get the id of the last room added to the sql table,
     * allow to create a model that has the correct ids.
     * @return the id of the last room added
     */
    public int getLastNotificationId() {
        open();
        /** get all the equipment from the table of the DB */
        Cursor c = mDb.rawQuery("select max(idNotification) from "+ TABLE_NAME, null);
        int id = -1;

        /** for each equipment create an object */
        while (c.moveToNext()) {
            id = c.getInt(0);
        }
        c.close();
        close();

        if(id!=-1)
            return id;
        else
            throw new NullPointerException();
    }
}
