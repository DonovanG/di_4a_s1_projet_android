\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Liste des intervenants}{a}{chapter*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Avertissement}{b}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Pour citer ce document}{c}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Table des mati\`eres\markboth {Table des mati\`eres}{Table des mati\`eres}}{i}{chapter*.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Table des figures}{iii}{chapter*.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Contexte de la r\IeC {\'e}alisation}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Objectifs}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Hypoth\IeC {\`e}ses}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Bases m\IeC {\'e}thodologiques}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Description g\IeC {\'e}n\IeC {\'e}rale}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Environnement du projet}{3}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Caract\IeC {\'e}ristiques des utilisateurs}{3}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Fonctionnalit\IeC {\'e}s du syst\IeC {\`e}me}{3}{section.2.3}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Structure g\IeC {\'e}n\IeC {\'e}rale du syst\IeC {\`e}me}{4}{section.2.4}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Interfaces externes}{6}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Interfaces mat\IeC {\'e}riel/logiciel}{6}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Interfaces homme/machine}{6}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Interfaces logiciel/logiciel}{8}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Sp\IeC {\'e}cifications fonctionnelles}{9}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Fonction connexion}{9}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Fonction envoyer requ\IeC {\^e}te TCP}{9}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Fonction ajouter une pi\IeC {\`e}ce}{9}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Fonction supprimer une pi\IeC {\`e}ce}{10}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Fonction ajouter un \IeC {\'e}quipement}{10}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Fonction supprimer un \IeC {\'e}quipement}{10}{section.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Fonction r\IeC {\'e}cup\IeC {\'e}ration des notifications}{11}{section.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Fonction r\IeC {\'e}cup\IeC {\'e}ration des Equipement en fonction de la cat\IeC {\'e}gorie}{11}{section.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Fonction r\IeC {\'e}cup\IeC {\'e}ration d'une pi\IeC {\`e}ce en fonction d'un equipement}{11}{section.4.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}Fonction r\IeC {\'e}cup\IeC {\'e}ration des pi\IeC {\`e}ces}{11}{section.4.10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Sp\IeC {\'e}cifications non fonctionnelles}{13}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Contraintes de d\IeC {\'e}veloppement et conception}{13}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Contraintes de fonctionnement et d\IeC {\textquoteright }exploitation}{13}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Performances}{13}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Capacit\IeC {\'e}s}{14}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}S\IeC {\'e}curit\IeC {\'e}}{14}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}\IeC {\'E}tapes accomplies}{15}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Mod\IeC {\'e}lisation}{15}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}D\IeC {\'e}veloppement}{15}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Interfaces}{15}{subsection.6.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Mod\IeC {\`e}le et fonctionnalit\IeC {\'e}s}{15}{subsection.6.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Test}{16}{section.6.3}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {french}
